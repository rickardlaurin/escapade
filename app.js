var app = angular.module("escapadeapp", ["google-maps"]);

app.controller("mapController", [ "$scope", function($scope) {
	//calc width of screen
	$('#wrapper').css({
		height : $(document).height(),
		width : $(document).width()
	});
	
	$('.view, .list-wrapper, #g-map').css({
		height : $(document).height() - 52,
		width: $(document).width()
	});

	$('#e-details').css({
		height: $(document).height() - 252
	})

	$('.list-wrapper').on('touchstart', function(){
   		if($(this).scrollTop() === 0){
   			$(this).scrollTop(1);
   		}
	})

	$('#get_started').on('touchend', function(e){
		e.preventDefault();
		$('#splash').css('-webkit-transform', 'translateY(100%)');
	})

	$scope.inView = 'map';
  $scope.showFilter = false;

	$scope.detailsID = undefined;
	$scope.centerPinLat = undefined;
	$scope.centerPinLon = undefined;
	$scope.events = undefined;

	$scope.changeView = function(view) {
		if (view !== 'filter')
			$scope.inView = view;
		else
			$scope.showFilter = $scope.showFilter ? false : true;
	}

	$scope.showDetails = function(id) {
		$scope.detailsID = id;
		$scope.changeView('details');
		$scope.centerPinLat = parseFloat($scope.events[id].latitude) - 0.007;
		$scope.centerPinLon = parseFloat($scope.events[id].longitude);

		console.log($scope.centerPinLat, $scope.centerPinLon);
	}

	$scope.$watch('centerPinLat', function() {
		$scope.$emit('listUpdated', $scope);
	});

	$scope.$on('listUpdated', function(scope) {
		$scope.events = scope.targetScope.events;

		var new_markers = [];

		scope.targetScope.events.forEach(function(k,v){
			new_markers[v] = {
				latitude: k.latitude,
				longitude: k.longitude,
				infoWindowContent: '<h3 class="name">'+k.name+'</h1><p class="date">'+k.start+'</p><p>'+k.location+'</p>',
				categories: k.categories.join(',')
			}
		});
		
		//pop map
		if (!$scope.centerPinLat && !$scope.centerPinLon) {
			angular.extend($scope, {
				center: {
					latitude: scope.targetScope.myPosition.latitude, // initial map center latitude
					longitude: scope.targetScope.myPosition.longitude // initial map center longitude
				},
				markers: new_markers, // an array of markers,
				zoom: 14, // the zoom level
			});
		}
		else {
			angular.extend($scope, {
				center: {
					latitude: $scope.centerPinLat, // initial map center latitude
					longitude: $scope.centerPinLon // initial map center longitude
				},
				markers: new_markers, // an array of markers,
				zoom: 14, // the zoom level
			});		
		}

		// show map
		$('.hide').css({
			'opacity' : 1
		});

	});
	
	// THIS IS FOR THIS TO WORK. FIX?
	//set pos of me
	angular.extend($scope, {
		center: {latitude: 50, longitude: 18},
		markers: [], // an array of markers,
		zoom: 8, // the zoom level
	});
	
}]);