function ListCtrl($scope, $http) {
	$scope.myPosition = {};
  $scope.events = [];

    $scope.myPosition = {
      "longitude": 18.06491,
      "latitude": 59.32893000000001
    };
    
    var apiURL = 'http://babbl.in:5001/?longitude={longitude}&latitude={latitude}';

    $http.get(apiURL.replace('{longitude}',$scope.myPosition.longitude).replace('{latitude}',$scope.myPosition.latitude))
        .success(function (data) {
          console.log(data)
          $scope.events = data['data'];
          $scope.events.forEach(function(eventy) {
            eventy.relativeStart = moment(eventy.start).calendar()

            eventy.categories.forEach(function(category) {
              if (!eventy.topCategory) {
                if (category === 'lastfm')
                  eventy.topCategory = 'concert';
                else if (category === 'food')
                  eventy.topCategory = 'food';
              }
            });
          });
          $scope.$emit('listUpdated', $scope);
        })
        .error(function(error) {
          console.log(error);
        }); 
}